package function

func Add(elemnts ...int) int {
	var sum int

	for _, element := range elemnts {
		sum += element
	}

	return sum
}

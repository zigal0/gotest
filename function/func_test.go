package function_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zigal0/gotest/function"
)

func Test_Add(t *testing.T) {
	t.Parallel()

	t.Run("base", func(t *testing.T) {
		t.Parallel()

		// arrange

		// act
		res := function.Add(10, 5)

		// assert
		require.Equal(t, 15, res)
	})
}
